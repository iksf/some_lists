#include <stdio.h>
#include <stdlib.h>
typedef struct Node
{
    int value;
    struct Node *next;
} Node;

typedef struct
{
    Node *head;
} LinkedList;

Node *gen_new_node(int value, Node *next)
{
    Node *a = (Node *)malloc(sizeof(Node));
    a->value = value;
    a->next = next;
    return a;
}

void push(LinkedList *list, int value)
{
    if (list->head == 0)
    {
        list->head = gen_new_node(value, NULL);
    }
    else
    {
        Node *new_node = gen_new_node(value, list->head);
        list->head = new_node;
    }
}
int pop(LinkedList *list)
{
    if (list->head == 0)
    {
        return 0;
    }
    Node *current = list->head;
    int temp = current->value;
    list->head = current->next;
    free(current);
    return temp;
}
void drop_list(LinkedList *list)
{
    while (list->head != 0)
    {
        pop(list);
    }
}

int main()
{
    LinkedList list;
    push(&list, 5);
    push(&list, 6);
    drop_list(&list);
    return 0;
}