//! Stack ADT
//! Kieran McKenzie
//! For program source code
//! press [src] over there ---------------------------> 
//! 
//! or press [src] next to any trait, struct, function etc for the code for that item

//  Import traits into scope
use std::fmt::{Debug, Display, Write};

/// Alias around the heap allocated and optional Node structure
/// That composes the list
pub type Link<T> = Option<Box<Node<T>>>;
/// Node structure contains the value of the item and the link to the next item in list
/// Generic over most things with a fixed Size at runtime (Sized trait) as well as things
/// that can be nicely formatted and presented to stdout (Display, Debug traits) 
/// and that have a concept of basic equality for the == operations (PartialEq trait)
// #[derive(Debug)] automatically implements the debug functionality
#[derive(Debug)]
pub struct Node<T> {
    value: T,
    next: Link<T>,
}

// Implements methods for this monomorphisation of Node for these 4 trait bounds
impl<T> Node<T>
where
    T: Sized + Debug + Display + PartialEq,
{   
    /// Method to make new node
    pub fn new(value: T, next: Link<T>) -> Self {
        Self { value, next }
    }
}
/// Structure for containing the head element of the list 
#[derive(Debug)]
pub struct LinkedListStack<T> {
    head: Link<T>,
}
// Implements methods for the List
impl<T> LinkedListStack<T>
where
    T: Sized + Debug + Display + PartialEq,
{   
    /// Creates a new Linked List
    pub fn new() -> Self {
        Self { head: None }
    }
    /// Checks if the list is empty and returns a bool if not
    pub fn is_empty(&self) -> bool {
        self.head.is_none()
    }
    /// Resets the list
    pub fn clear(&mut self) {
        *self = LinkedListStack::new()
    }

    /// Adds a new element to the list
    pub fn push(&mut self, value: T) {
        let new_link = Some(Box::new(
            Node::new(value, std::mem::replace(&mut self.head, None)),
        ));

        self.head = new_link;
    }
    /// Removes an element from the list, returning an optional that must be 
    /// processed to ensure the element is not NULL
    pub fn pop(&mut self) -> Option<T> {
        // Limitation of safe Rust is it will not allow me to leave the head empty, so I must change it 
        // to the other enum varient rather than just leaving it as a null-pointer
        // Only 1 bit worth of inefficiency but with unsafe I could make a small improvement in speed
        // and ensures no dangling pointer
        let current = std::mem::replace(&mut self.head, None);
        if let Some(mut boxed_node) = current {
            self.head = std::mem::replace(&mut boxed_node.next, None);
            Some(boxed_node.value)
        } else {
            None
        }
    }
    /// Returns true if an element is found in the list
    pub fn has_a(&self, value: T) -> bool {
        self.iter().any(|x| *x == value)
    }
    /// Creates an iterator structure for the list
    /// this iterator structure can be used to iterate over the linked list
    /// without knowledge of its internal structure
    /// This iterator is a read-only version of the structure
    pub fn iter<'a>(&'a self) -> Iter<'a, T> {
        Iter {
            next: self.head.as_ref().map(|x| &**x),
        }
    }
    /// Creates a mutable iterator
    /// Same as the regular iterator structure however allows read-write of the elements
    /// Avoiding using to avoid changes to the list, use .iter() instead
    pub fn iter_mut<'a>(&'a mut self) -> IterMut<'a, T> {
        IterMut {
            next: self.head.as_mut().map(|x| &mut **x),
        }
    }
    /// Uses a mutable iterator to apply an anonymous function (closure) to every element in the list
    /// The closure can actually be any function that modifies the element,
    /// however for the purpose of the UI only adding is supported
    /// This closure is heap allocated due to stack sizing requirement, and is hidden behind a smart pointer
    pub fn add_to_all(&mut self, func: Box<Fn(&mut T)>) {
        for i in self.iter_mut() {
            func(i)
        }
    }
    /// This uses the non mutable iterator and a closure to return a string containing the positions of every element
    /// that matches the closure
    /// Any closure that returns a bool is valid but for the purpose of the UI only direct equivilence is used
    pub fn get_index_of_all(&self, func: Box<Fn(&T) -> bool>) -> String {
        let mut string = "Found at: ".to_owned();
        for value in self.iter()
            .enumerate() //Gives me an index value to work with
            .filter(|&(_, x)| func(x)) //If the value meets the criteria of the function, continue, else discard
            .map(|(i, _)| i) //Take the value
            .collect::<Vec<usize>>() //Collect it as a vector
            .iter() //Iterate over this new vector
        {
            // For each format a string and add it to my output string
            let mut current = String::new();
            write!(&mut current, "{} ", value).unwrap();
            string += &current
        }

        string
    }
    /// Returns an integer which is the size of the list
    pub fn length(&self) -> usize {
        self.iter().count()
    }
    /// This function removes elements from the list that meets the criteria
    /// This is an unsafe function, meaning im ignoring the safety rules of rust to implement this function
    /// I could implement it with safe code but yea time
    /// This unsafe code does deal with raw pointers and doesnt check for invalid pointers
    /// Some small issues with a few niche cases however, but just deleting based on equivilence works
    pub unsafe fn delete_where(&mut self, func: Box<Fn(&T) -> bool>) {
        //Convert my mutable reference into a raw pointer
        let mut current = &mut self.head as *mut Link<T>; 
        //Check there's actually something at the address, pointed to by the pointer, then deref it
        if let Some(ref mut boxed) = *current {
            // if the closure returns true with the input from this Node
            if func(&boxed.value) {
                //Swap the memory at a bit level to the other enum varient None
                self.head = std::mem::replace(&mut boxed.next, None);
            }
        }
        loop {
            //For each pointer, check the location is actually a Node
            //Then check it meets the criteria of the passed in closure
            //If so, take replace the reference with None and continue
            if let Some(ref mut pointer) = *current {
                if let Some(ptr) = {
                    if let Some(ref mut next_box) = pointer.next {
                        if func(&next_box.value) {
                            Some(std::mem::replace(&mut next_box.next, None))
                        } else {
                            None
                        }
                    } else {
                        None
                    }
                } {
                    //To here where I replace the existing reference with the one stolen from
                    //next node
                    std::mem::replace(&mut pointer.next, ptr);
                }
                //Move to next node
                current = &mut pointer.next as *mut Link<T>;
            } else {
                break;
            }
        }
    }
}
/// This structure can be generated to provide a lazy iterator with which you can read the list
/// Generic over type T and the 'a is the lifetime mapping of its references, as Rust enforces RAII
pub struct Iter<'a, T: 'a> {
    next: Option<&'a Node<T>>,
}
/// This structure provides a lazy iterator to edit the list
pub struct IterMut<'a, T: 'a> {
    next: Option<&'a mut Node<T>>,
}

/// Implementation of the std Iterator trait for my Iter item
/// By simply implementing the next method correctly I get all the functions in the Rust std::iter module
/// for free on my structure
impl<'a, T: Sized + Debug + Display + PartialEq> Iterator for Iter<'a, T> {
    /// Type of item yeilded by the iterator
    type Item = &'a T;
    /// trait method for implementing the Iterator trait for this structure
    fn next(&mut self) -> Option<Self::Item> {
        self.next.map(|x| {
            self.next = x.next.as_ref().map(|y| &**y);
            &x.value
        })
    }
}
/// Implementation of Iterator for my IterMut structure
impl<'a, T: Sized + Debug + Display + PartialEq> Iterator for IterMut<'a, T> {
    type Item = &'a mut T;
    fn next(&mut self) -> Option<Self::Item> {
        self.next.take().map(|x| {
            self.next = x.next.as_mut().map(|y| &mut **y);
            &mut x.value
        })
    }
}


/// Little function to handle text input
pub fn read_line() -> String {
    let mut string = String::new();
    std::io::stdin().read_line(&mut string).unwrap();
    string
}
/// Main function
pub fn main() {
    let mut int_list: LinkedListStack<isize> = LinkedListStack::new();
    let help = "
        commands:
        isEmpty             checks if the list is empty
        add x               adds x to list where x is int
        list                displays the list
        length              displays list length
        pop                 removes last element in list
        delete x            removes all elements matching x
        has x               checks if list contains an element x
        find x              finds indices of all occurances of x
        add_to_all x        adds x to every item in the list          
        clear               resets list
        help                shows this help
        exit                ends program
    ";
    println!("{}", help);
    'main_loop: loop {

        //Split whitespace and store as string vector
        let input = read_line()
            .trim()
            .split_whitespace()
            .map(|x| x.trim().to_owned())
            .collect::<Vec<String>>();
        match &*input[0] {
            "isEmpty" => if int_list.is_empty() {
                println!("List is empty")
            } else {
                println!("List is not empty")
            },
            "add" => if let Ok(value) = input[1].parse::<isize>() {
                int_list.push(value);
            } else {
                println!("error");
                continue;
            },
            "list" => {
                //Copy every element of the list into a vector and present it to the user
                println!(
                    "{:?}",
                    int_list.iter().map(|x| x.clone()).collect::<Vec<isize>>()
                );
            }
            "pop" => if let Some(value) = int_list.pop() {
                println!("Item removed: {}", value);
            } else {
                println!("list empty");
            },
            "has" => {
                if let Ok(value) = input[1].parse::<isize>() {
                    if int_list.has_a(value) {
                        println!("List does contain {}", value);
                    }       
                    else {
                        println!("Not found in list")
                    }
                }
            }
            "find" => if let Ok(value) = input[1].parse::<isize>() {
                println!(
                    "{}",
                    int_list.get_index_of_all(Box::new(move |x: &isize| -> bool { *x == value }))
                );
            },
            "delete" => if let Ok(value) = input[1].parse::<isize>() {
                println!("Removing all instances of {}", value);
                //Unsafe call to access unsafe function

                unsafe {
                    //Heap allocates a function pointer with Box that matches the type signature &T -> bool
                    int_list.delete_where(Box::new(move |x: &isize| -> bool { *x == value }));
                }
            },
            "add_to_all" => if let Ok(value) = input[1].parse::<isize>() {
                int_list.add_to_all(Box::new(move |x: &mut isize| *x += value));
            },
            "length" => {
                println!("Number of items in stack {}", int_list.length());
            },
            "clear" => {
                int_list.clear();
                println!("List cleared");
            },
            "exit" => break 'main_loop,
            "help" => println!("{}", help),
            _ => continue,
        }
    }
}
