use std::fmt::{Debug, Display, Write};
type Link<T> = Option<Box<Node<T>>>;
#[derive(Debug)]
struct Node<T> {
    value: T,
    next: Link<T>,
}

impl<T> Node<T>
where
    T: Sized + Debug + Display + PartialEq,
{
    pub fn new(value: T, next: Link<T>) -> Self {
        Self { value, next }
    }
}
#[derive(Debug)]
struct LinkedListStack<T> {
    head: Link<T>,
}

impl<T> LinkedListStack<T>
where
    T: Sized + Debug + Display + PartialEq,
{
    pub fn new() -> Self {
        Self { head: None }
    }

    pub fn push(&mut self, value: T) {
        let new_link = Some(Box::new(
            Node::new(value, std::mem::replace(&mut self.head, None)),
        ));

        self.head = new_link;
    }

    pub fn pop(&mut self) -> Option<T> {
        let current = std::mem::replace(&mut self.head, None);
        if let Some(mut boxed_node) = current {
            self.head = std::mem::replace(&mut boxed_node.next, None);
            Some(boxed_node.value)
        } else {
            None
        }
    }

    pub fn iter<'a>(&'a self) -> Iter<'a, T> {
        Iter {
            next: self.head.as_ref().map(|x| &**x),
        }
    }
    pub fn iter_mut<'a>(&'a mut self) -> IterMut<'a, T> {
        IterMut {
            next: self.head.as_mut().map(|x| &mut **x),
        }
    }

    pub fn get_index_of_all(&self, func: Box<Fn(&T) -> bool>) -> String {
        let mut string = "Found at: ".to_owned();
        for value in self.iter()
            .enumerate()
            .filter(|&(_, x)| func(x))
            .map(|(i, _)| i)
            .collect::<Vec<usize>>()
            .iter()
        {
            let mut current = String::new();
            write!(&mut current, "{} ", value);
            string += &current
        }

        string
    }

    pub fn length(&self) -> usize {
        self.iter().count()
    }

    pub fn delete_where(&mut self, func: Box<Fn(&T) -> bool>) {
        let mut current = self.head.as_mut();

        loop {
            if let Some(mut boxed) = current.as_mut() {
                if let Some(mut ptr) = {
                    if let Some(mut next_box) = boxed.next.as_mut() {
                        if func(&next_box.value) {
                            let output = std::mem::replace(&mut next_box.next, None);
                            Some(output)
                        } else {
                            None
                        }
                    } else {
                        return;
                    }
                } {
                    std::mem::replace(&mut boxed.next, ptr);
                }
            }

            //            current = &mut current.as_ref().next;
        }
    }
}

struct Iter<'a, T: 'a> {
    next: Option<&'a Node<T>>,
}
struct IterMut<'a, T: 'a> {
    next: Option<&'a mut Node<T>>,
}


impl<'a, T: Sized + Debug + Display + PartialEq> Iterator for Iter<'a, T> {
    type Item = &'a T;
    fn next(&mut self) -> Option<Self::Item> {
        self.next.map(|x| {
            self.next = x.next.as_ref().map(|y| &**y);
            &x.value
        })
    }
}
impl<'a, T: Sized + Debug + Display + PartialEq> Iterator for IterMut<'a, T> {
    type Item = &'a mut T;
    fn next(&mut self) -> Option<Self::Item> {
        self.next.take().map(|x| {
            self.next = x.next.as_mut().map(|y| &mut **y);
            &mut x.value
        })
    }
}

fn main() {
    let mut stack = LinkedListStack::new();
    for i in 0..10 {
        stack.push(i);
    }




    //    let mut string = String::new();
    //    println!("{}", stack.locate(4));
    //    stack.find(4);
}
