class List(object): 

    def __init__ (self):
        self.head = None

    def push(self, data): 
        new_node = Node(data, self.head)
        self.head = new_node
    
    def pop(self): 
        output = self.head.value
        self.head = self.head.next
        return output
        
    def drop(self, value):
        current = self.head
        while current.next is not None:
            next = current.next
            if next.value == value:
                current.next = next.next
            current = next

        if self.head.value == value:
            self.head = current.next
             
    def search(self, value): 
        if self.head.value == value:
            return 0

        current = self.head
        i = 0
        while current.next is not None:
            next = current.next
            if next.value == value:
                return i
        
            current = next
            i+=1

        
class Node: 
    def __init__(self, data, next):
        self.value = data
        self.next = next

a = List()

for i in range(0, 10):
    a.push(i)

print(a.search(3))
#a.drop(4)
#for i in range(0, 10):
#    print(a.pop())
